# XDesign Munro Challenge

This is a solution to the XDesign Munro Challenge. Developed using Spring Boot 2.4.2 and Java 11. 

## Installation

To run the application you can run it from the command line 

```bash
cd application directory
./mvn spring-boot:run
```

Or using your IDE, open the MunroChallengeApplication file, and run it as Java Application.

You will see a message saying that the application has started on port 8080

## Usage

The application uses OPEN API 3.0 and Swagger to document the available API. Once the application has started, you are able to view the documentation at
[here](localhost:8080/swagger-ui/index.html?configUrl=/api-docs/swagger-config) 
It's worth noting you should clear the results before executing again as it appears to cache sometimes.
![alt text](https://crcsoftwaresolutions.com/xdesign_swagger.png "Swagger screenshot")


## Logging
The default logging level is currently set to debug. However this can be set within the application.properties

## Tests
To run the tests, you can run either of SearchResultsTest or InvalidSearchTermsTest classes using an IDE. Or from the 
command line. Expected tests results were calculated using Excel

```bash
mvn test
```
