package com.xdesign.munro;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.hamcrest.Matchers.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.xdesign.munro.controllers.MunroController;
import com.xdesign.munro.model.ResponseObject;
import com.xdesign.munro.services.LoadService;
import com.xdesign.munro.services.SearchService;
import com.xdesign.munro.services.StartupService;
import com.xdesign.munro.services.UtilService;


@AutoConfigureMockMvc
@ContextConfiguration(classes = {MunroController.class, LoadService.class, SearchService.class, 
		StartupService.class, UtilService.class, ResponseObject.class})
@WebMvcTest
public class SearchResultsTest {

	@Autowired
	MockMvc mockMvc;

	@Autowired
	ObjectMapper objectMapper;
	
	private final static String API_URL = "/searchmunros";
	private final static int ALL_MUNROS = 509;

	/**
	 * Simple test of the /munros call which just returns all Munro's with a category
	 */
	@Test
	public void allMunroTest() throws Exception {
		MvcResult result = mockMvc.perform(MockMvcRequestBuilders.get("/munros")
				.accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk())
				.andReturn();

		String actualResponseBody = result.getResponse().getContentAsString();

		assertNotNull(actualResponseBody);		  

	}

	/**
	 * Simple search of the /searchmunros API which uses default for all parameters
	 */
	@Test
	public void defaultSearch() throws Exception {	
		mockMvc.perform(MockMvcRequestBuilders.get(API_URL)
				.accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk())
				.andExpect(jsonPath("$.data").isArray())
				.andExpect(jsonPath("$.data", hasSize(ALL_MUNROS)))
				.andExpect(jsonPath("$.data[0].name", is("Ben Chonzie")))
				.andReturn();
	}
	
	/**
	 * search by category MUNRO and nothing else
	 */
	@Test
	public void searchByCatMunro() throws Exception {	
		mockMvc.perform(MockMvcRequestBuilders.get(API_URL+"?category=MUN")
				.accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk())
				.andExpect(jsonPath("$.data").isArray())
				.andExpect(jsonPath("$.data", hasSize(282)))
				.andExpect(jsonPath("$.data[1].name", is("Ben Vorlich")))
				.andReturn();
	}
	
	/**
	 * search by category TOP and nothing else
	 */
	@Test
	public void searchByCatTop() throws Exception {	
		mockMvc.perform(MockMvcRequestBuilders.get(API_URL+"?category=TOP")
				.accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk())
				.andExpect(jsonPath("$.data").isArray())
				.andExpect(jsonPath("$.data", hasSize(227)))
				.andExpect(jsonPath("$.data[0].name", is("Stob Binnein - Stob Coire an Lochain")))
				.andReturn();
	}
	
	/**
	 * search by category EITHER (default) and nothing else
	 */
	@Test
	public void searchByCatEither() throws Exception {	
		mockMvc.perform(MockMvcRequestBuilders.get(API_URL+"?category=EITHER")
				.accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk())
				.andExpect(jsonPath("$.data").isArray())
				.andExpect(jsonPath("$.data", hasSize(ALL_MUNROS)))
				.andExpect(jsonPath("$.data[0].name", is("Ben Chonzie")))
				.andReturn();
	}
	
	/**
	 * Apply a sort of height asc
	 */
	@Test
	public void searchByHeightAsc() throws Exception {	
		mockMvc.perform(MockMvcRequestBuilders.get(API_URL+"?sortby=HEIGHT&sortorder=ASC")
				.accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk())
				.andExpect(jsonPath("$.data").isArray())
				.andExpect(jsonPath("$.data", hasSize(ALL_MUNROS)))
				.andExpect(jsonPath("$.data[0].name", is("Mullach Coire nan Cisteachan [Carn na Caim South Top]")))
				.andExpect(jsonPath("$.data[1].name", is("Beinn Teallach")))
				.andExpect(jsonPath("$.data[2].name", is("Sgurr nan Ceathreamhnan - Stob Coire na Cloiche")))
				.andReturn();
	}
	
	/**
	 * Apply a sort of height desc
	 */
	@Test
	public void searchByHeightDesc() throws Exception {	
		mockMvc.perform(MockMvcRequestBuilders.get(API_URL+"?sortby=HEIGHT&sortorder=DESC")
				.accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk())
				.andExpect(jsonPath("$.data").isArray())
				.andExpect(jsonPath("$.data", hasSize(ALL_MUNROS)))
				.andExpect(jsonPath("$.data[0].name", is("Ben Nevis")))
				.andExpect(jsonPath("$.data[1].name", is("Ben Macdui [Beinn Macduibh]")))
				.andExpect(jsonPath("$.data[2].name", is("Braeriach")))
				.andReturn();
	}
	
	/**
	 * Apply a sort of name asc
	 */
	@Test
	public void searchByNameAsc() throws Exception {	
		mockMvc.perform(MockMvcRequestBuilders.get(API_URL+"?sortby=NAME&sortorder=ASC")
				.accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk())
				.andExpect(jsonPath("$.data").isArray())
				.andExpect(jsonPath("$.data", hasSize(ALL_MUNROS)))
				.andExpect(jsonPath("$.data[0].name", is("A' Bhuidheanach Bheag")))
				.andExpect(jsonPath("$.data[1].name", is("A' Bhuidheanach Bheag - Glas Mheall Mor")))
				.andExpect(jsonPath("$.data[2].name", is("A' Chailleach")))
				.andReturn();
	}
	
	/**
	 * Apply a sort of name desc
	 */
	@Test
	public void searchByNameDesc() throws Exception {	
		mockMvc.perform(MockMvcRequestBuilders.get(API_URL+"?sortby=NAME&sortorder=DESC")
				.accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk())
				.andExpect(jsonPath("$.data").isArray())
				.andExpect(jsonPath("$.data", hasSize(ALL_MUNROS)))
				.andExpect(jsonPath("$.data[0].name", is("Tom Buidhe")))
				.andExpect(jsonPath("$.data[1].name", is("Tom a' Choinich - Tom a' Choinich Beag")))
				.andExpect(jsonPath("$.data[2].name", is("Tom a' Choinich - An Leth-chreag")))
				.andReturn();
	}
	
	/**
	 * Search for category MUN Apply a sort of height desc
	 */
	@Test
	public void searchByMunroHeightDesc() throws Exception {	
		mockMvc.perform(MockMvcRequestBuilders.get(API_URL+"?category=MUN&sortby=HEIGHT&sortorder=DESC")
				.accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk())
				.andExpect(jsonPath("$.data").isArray())
				.andExpect(jsonPath("$.data", hasSize(282)))
				.andExpect(jsonPath("$.data[0].name", is("Ben Nevis")))
				.andExpect(jsonPath("$.data[1].name", is("Ben Macdui [Beinn Macduibh]")))
				.andExpect(jsonPath("$.data[2].name", is("Braeriach")))
				.andReturn();
	}
	
	/**
	 * limit results to 10
	 */
	@Test
	public void searchByLimit() throws Exception {	
		mockMvc.perform(MockMvcRequestBuilders.get(API_URL+"?limit=10")
				.accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk())
				.andExpect(jsonPath("$.data").isArray())
				.andExpect(jsonPath("$.data", hasSize(10)))
				.andExpect(jsonPath("$.data[0].name", is("Ben Chonzie")))
				.andReturn();
	}
	
	/**
	 * limit results to 10 and cat = MUN and sort by height ASC 
	 */
	@Test
	public void searchByCatWithSortLimit() throws Exception {	
		mockMvc.perform(MockMvcRequestBuilders.get(API_URL+"?category=MUN&sortby=HEIGHT&sortorder=ASC&limit=10")
				.accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk())
				.andExpect(jsonPath("$.data").isArray())
				.andExpect(jsonPath("$.data", hasSize(10)))
				.andExpect(jsonPath("$.data[9].name", is("Ruadh Stac Mor")))
				.andReturn();
	}
	
	/**
	 * search by a min height 1000
	 */
	@Test
	public void searchByMinHeight() throws Exception {	
		mockMvc.perform(MockMvcRequestBuilders.get(API_URL+"?minheight=1000")
				.accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk())
				.andExpect(jsonPath("$.data").isArray())
				.andExpect(jsonPath("$.data", hasSize(220)))
				.andExpect(jsonPath("$.data[0].name", is("Ben More")))
				.andExpect(jsonPath("$.data[1].name", is("Stob Binnein")))
				.andExpect(jsonPath("$.data[2].name", is("Stob Binnein - Stob Coire an Lochain")))
				.andReturn();
	}
	
	/**
	 * search by a max height 1000
	 */
	@Test
	public void searchByMaxHeight() throws Exception {	
		mockMvc.perform(MockMvcRequestBuilders.get(API_URL+"?maxheight=1000")
				.accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk())
				.andExpect(jsonPath("$.data").isArray())
				.andExpect(jsonPath("$.data", hasSize(288)))
				.andExpect(jsonPath("$.data[0].name", is("Ben Chonzie")))
				.andExpect(jsonPath("$.data[1].name", is("Ben Vorlich")))
				.andExpect(jsonPath("$.data[2].name", is("Stuc a' Chroin")))
				.andReturn();
	}
	
	/**
	 * search by a min height 1000 and max 1100
	 */
	@Test
	public void searchByMinMaxHeight() throws Exception {	
		mockMvc.perform(MockMvcRequestBuilders.get(API_URL+"?minheight=1000&maxheight=1100")
				.accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk())
				.andExpect(jsonPath("$.data").isArray())
				.andExpect(jsonPath("$.data", hasSize(145)))
				.andExpect(jsonPath("$.data[0].name", is("Stob Binnein - Stob Coire an Lochain")))
				.andExpect(jsonPath("$.data[1].name", is("Cruach Ardrain")))
				.andExpect(jsonPath("$.data[2].name", is("Ben Oss")))
				.andReturn();
	}
	
	/**
	 * Search using everything
	 */
	@Test
	public void searchByEverything() throws Exception {	
		mockMvc.perform(MockMvcRequestBuilders.get(API_URL+"?category=MUN&sortby=HEIGHT"
				+ "&sortorder=ASC&limit=10&minheight=1000&maxheight=1300")
				.accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk())
				.andExpect(jsonPath("$.data").isArray())
				.andExpect(jsonPath("$.data", hasSize(10)))
				.andExpect(jsonPath("$.data[0].name", is("Meall Greigh")))
				.andExpect(jsonPath("$.data[1].name", is("Beinn a' Bheithir - Sgorr Dhonuill")))
				.andExpect(jsonPath("$.data[2].name", is("Aonach Meadhoin")))
				.andReturn();
	}
	
	/**
	 * tests below are for more complex sorts
	 */
	
	/**
	 * sort by name then height both asc. A' Chailleach shows it working
	 */
	@Test
	public void searchByNameThenHeight() throws Exception {	
		mockMvc.perform(MockMvcRequestBuilders.get(API_URL+"?sortby=NAME_HEIGHT&sortorder=ASC_ASC")
				.accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk())
				.andExpect(jsonPath("$.data").isArray())
				.andExpect(jsonPath("$.data", hasSize(ALL_MUNROS)))
				.andExpect(jsonPath("$.data[2].name", is("A' Chailleach")))
				.andExpect(jsonPath("$.data[2].height", is(929.2)))
				.andExpect(jsonPath("$.data[3].name", is("A' Chailleach")))
				.andExpect(jsonPath("$.data[3].height", is(997.0)))
				.andReturn();
	}
	
	/**
	 * sort by height desc then name asc
	 */
	@Test
	public void searchByHeightThenName() throws Exception {	
		mockMvc.perform(MockMvcRequestBuilders.get(API_URL+"?sortby=HEIGHT_NAME&sortorder=DESC_ASC")
				.accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk())
				.andExpect(jsonPath("$.data").isArray())
				.andExpect(jsonPath("$.data", hasSize(ALL_MUNROS)))
				.andExpect(jsonPath("$.data[8].name", is("Aonach Mor")))
				.andExpect(jsonPath("$.data[8].height", is(1221.0)))
				.andExpect(jsonPath("$.data[9].name", is("Ben Nevis - Carn Dearg NW Top (new GR)")))
				.andExpect(jsonPath("$.data[9].height", is(1221.0)))
				.andReturn();
	}
	


}
