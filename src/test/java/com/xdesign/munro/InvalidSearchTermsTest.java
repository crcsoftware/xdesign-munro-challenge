package com.xdesign.munro;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.xdesign.munro.controllers.MunroController;
import com.xdesign.munro.model.ResponseObject;
import com.xdesign.munro.services.LoadService;
import com.xdesign.munro.services.SearchService;
import com.xdesign.munro.services.StartupService;
import com.xdesign.munro.services.UtilService;


@AutoConfigureMockMvc
@ContextConfiguration(classes = {MunroController.class, LoadService.class, SearchService.class, 
		StartupService.class, UtilService.class, ResponseObject.class})
@WebMvcTest
public class InvalidSearchTermsTest {

	@Autowired
	MockMvc mockMvc;

	@Autowired
	ObjectMapper objectMapper;

	private final static String INVALID_CAT_MESSAGE = "Error, invalid category supplied, should be TOP, MUNRO or EITHER";
	private final static String INVALID_MAX_HEIGHT_MESSAGE = "Error, max height supplied is not an integer";
	private final static String INVALID_MIN_HEIGHT_MESSAGE = "Error, min height supplied is not an integer";
	private final static String INVALID_LIMIT_MESSAGE = "Error, limit supplied is not an integer";
	private final static String INVALID_SORT_BY_MESSAGE = "Error, invalid sort by has been supplied, "
			+ "should be NAME, HEIGHT, NAME_HEIGHT, HEIGHT_NAME or NONE";
	private final static String INVALID_COMPLEX_SORT_ORDER_MESSAGE = "Error, invalid sort order supplied for a complex sort";
	private final static String INVALID_SIMPLE_SORT_ORDER_MESSAGE = "Error, invalid sort order supplied";
	private final static String INVALID_MIN_MAX_MESSAGE = "Error, the max height cannot be less than the min height";
	
	@Test
	public void allMunroTest() throws Exception {
		MvcResult result = mockMvc.perform(MockMvcRequestBuilders.get("/munros")
				.accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk())
				.andReturn();

		String actualResponseBody = result.getResponse().getContentAsString();

		assertNotNull(actualResponseBody);		  

	}

	@Test
	public void invalidCategory() throws Exception {		
		runInvalidTest("?category=TOPPPPP",INVALID_CAT_MESSAGE);	
	}

	@Test
	public void invalidMaxHeight() throws Exception {
		runInvalidTest("?maxheight=Not_a_number",INVALID_MAX_HEIGHT_MESSAGE);
	}

	@Test
	public void invalidMinHeight() throws Exception {
		runInvalidTest("?minheight=-4",INVALID_MIN_HEIGHT_MESSAGE);
	}
	
	@Test
	public void invalidLimit() throws Exception {
		runInvalidTest("?limit=Not_a_number",INVALID_LIMIT_MESSAGE);
	}
	
	@Test
	public void invalidSortBy() throws Exception {
		runInvalidTest("?sortby=invalid",INVALID_SORT_BY_MESSAGE);
	}
	
	@Test
	public void invalidComplexSortOrder() throws Exception {
		runInvalidTest("?sortby=NAME_HEIGHT&sortorder=INVALID",INVALID_COMPLEX_SORT_ORDER_MESSAGE);
	}
	
	@Test
	public void invalidSimpleSortOrder() throws Exception {
		runInvalidTest("?sortby=NAME&sortorder=INVALID",INVALID_SIMPLE_SORT_ORDER_MESSAGE);
	}
	
	@Test
	public void invalidMinMaxHeight() throws Exception {
		runInvalidTest("?minheight=100&maxheight=50",INVALID_MIN_MAX_MESSAGE);
	}

	private String removeWhiteSpaces(String input) {
		return input.replaceAll("\\s+", "");
	}
	
	private void runInvalidTest(String url, String expectedMessage) throws Exception {
		MvcResult result = mockMvc.perform(MockMvcRequestBuilders.get("/searchmunros"+url)
				.accept(MediaType.APPLICATION_JSON))
				.andExpect(status().is4xxClientError())
				.andReturn();

		ResponseObject ro = new ResponseObject(removeWhiteSpaces(expectedMessage));

		String actualResponseBody = result.getResponse().getContentAsString();

		assertEquals(removeWhiteSpaces(actualResponseBody), removeWhiteSpaces(objectMapper.writeValueAsString(ro)));	
	}

}
