package com.xdesign.munro.exceptions;


/**
 * An exception class which is used to show a message directly to the user
 */
public class ApplicationException extends Exception { 

	private static final long serialVersionUID = 1671117428970445290L;

	public ApplicationException(String errorMessage) {
		super(errorMessage);
	}

}

