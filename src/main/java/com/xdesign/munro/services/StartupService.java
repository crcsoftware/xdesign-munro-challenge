package com.xdesign.munro.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import com.xdesign.munro.controllers.MunroController;

/**
 * This class runs on start up of the application and is used for 
 * storing the list of Munro's in the application context
 */
@Component
public class StartupService implements ApplicationRunner {

	Logger logger = LoggerFactory.getLogger(MunroController.class);

	private LoadService loadService;

	@Autowired
	public StartupService(LoadService loadService) {
		this.loadService = loadService;
	}

	@Override
	public void run(ApplicationArguments args) throws Exception {
		logger.info("Starting the load from CSV file on application startup");
		loadService.loadFromCsv();
		logger.info("Completed the load from CSV file on application startup");
	}

}
