package com.xdesign.munro.services;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletContext;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.ResourceUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.context.ServletContextAware;

import com.xdesign.munro.controllers.MunroController;
import com.xdesign.munro.exceptions.ApplicationException;
import com.xdesign.munro.model.Constants;
import com.xdesign.munro.model.Munro;

@Component
public class LoadService implements ServletContextAware {

	Logger logger = LoggerFactory.getLogger(MunroController.class);
	
	private UtilService utilService;
	private ServletContext context;

	private final static String CSV_FILE_NAME = "classpath:static/munrotab_v6.2.csv";

	@Autowired
	public LoadService(UtilService utilService) {
		this.utilService = utilService;
	}

	public void setServletContext(ServletContext servletContext) {
		this.context = servletContext;
	}

	/**
	 * Used just to check if Munro's are already stored in context
	 * @return true if list exists in context
	 */
	public boolean munrosStoredAlready() {
		if (context.getAttribute(Constants.STORED_MUNROS) != null) {
			return true;
		} else {
			return false;
		}
	}
	
	/**
	 * Gets the list of Munro's stored within the context
	 * @return the list of all Munro's
	 */
	@SuppressWarnings("unchecked")
	public List<Munro> getMunrosFromContext() {
		return (List<Munro>) context.getAttribute(Constants.STORED_MUNROS);
	}

	/**
	 * Utility method which reads from the CSV file and stores the list of Munro's 
	 * within the application<br>
	 * If the Munro does not have a category in 1997 then it's not added to list<br>
	 */
	public void loadFromCsv() throws Exception {

		File csvFile;
		try {
			csvFile = ResourceUtils.getFile(CSV_FILE_NAME);
		} catch (IOException ex) {
			throw new ApplicationException("Error, CSV file has not been found");
		}

		logger.debug("Located the file");

		if (!csvFile.canRead()) {
			throw new ApplicationException("Error, unable to read CSV file");
		}

		logger.debug("Able to read the file");

		List<Munro> munros = new ArrayList<>();

		Reader in = new FileReader(csvFile);
		Iterable<CSVRecord> records = CSVFormat.EXCEL.withFirstRecordAsHeader().parse(in);

		for (CSVRecord record : records) {
			boolean validMunro = true;

			String name = record.get(Constants.CSV_HEADING_NAME);
			if (!StringUtils.hasText(name)) {
				validMunro = false;
				logger.debug("Not going to add munro as no name");
			}

			String height = record.get(Constants.CSV_HEADING_HEIGHT);
			if (!StringUtils.hasText(height)) {
				validMunro = false;
				logger.debug("Not going to add munro as no height in metres");
			} else {
				if (!utilService.isValidDouble(height)) {
					validMunro = false;
					logger.debug("Not going to add munro as height not a number");
				}
			}

			String category = record.get(Constants.CSV_HEADING_CAT);
			if (!StringUtils.hasText(category)) {
				validMunro = false;
				logger.debug("Not going to add munro as no category specified");
			}

			String gridReference = record.get(Constants.CSV_HEADING_GRID);
			if (!StringUtils.hasText(gridReference)) {
				validMunro = false;
				logger.debug("Not going to add munro as no grid reference");
			}

			if (validMunro) {
				munros.add(new Munro(name, gridReference, category, utilService.getDoubleValue(height)));
			}
		}
		logger.debug("About to set Munro list to context");
		context.setAttribute(Constants.STORED_MUNROS, munros);
	}


}
