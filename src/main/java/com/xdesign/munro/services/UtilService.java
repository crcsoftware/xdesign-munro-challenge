package com.xdesign.munro.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.xdesign.munro.controllers.MunroController;

@Service
public class UtilService {

	Logger logger = LoggerFactory.getLogger(MunroController.class);

	/**
	 * Utility method for checking a string is an int
	 * @param toCheck
	 * @return  true if valid int else false
	 */
	public boolean isValidInteger(String toCheck) {
		try {
			Integer.parseInt(toCheck);
			return true;
		} catch (NumberFormatException nfe) {
			return false;
		}
	}

	/**
	 * Utility method for getting the int value from a string
	 * @param toconvert
	 * @return the int value or 0 if error
	 */
	public int getIntValue(String toconvert) {
		try {
			return Integer.parseInt(toconvert);
		} catch (NumberFormatException nfe) {
			return 0;
		}
	}

	/**
	 * Utility method for checking a string is a double
	 * @param toCheck
	 * @return  true if valid double else false
	 */
	public boolean isValidDouble(String toCheck) {
		try {
			Double.parseDouble(toCheck);
			return true;
		} catch (NumberFormatException nfe) {
			return false;
		}
	}

	/**
	 * Utility method for getting the double value from a string
	 * @param toconvert
	 * @return the double value or 0 if error
	 */
	public double getDoubleValue(String toconvert) {
		try {
			return Double.parseDouble(toconvert);
		} catch (NumberFormatException nfe) {
			return 0;
		}
	}

	/** 
	 * Checks that a number isn't negative
	 * @param toCheck
	 * @return
	 */
	public boolean isNegative(double toCheck) {
		if (toCheck < 0) {
		    return true;
		} else {
		    return false;
		}
	}

}