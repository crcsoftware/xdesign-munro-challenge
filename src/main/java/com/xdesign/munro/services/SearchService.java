package com.xdesign.munro.services;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import org.apache.commons.lang3.EnumUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.xdesign.munro.controllers.MunroController;
import com.xdesign.munro.exceptions.ApplicationException;
import com.xdesign.munro.model.Category;
import com.xdesign.munro.model.Constants;
import com.xdesign.munro.model.Munro;
import com.xdesign.munro.model.SearchParameters;
import com.xdesign.munro.model.SortBy;
import com.xdesign.munro.model.SortOrder;
import com.xdesign.munro.model.SortOrderComplex;

@Service
public class SearchService {

	Logger logger = LoggerFactory.getLogger(MunroController.class);

	private UtilService utilService;
	private LoadService loadService;

	@Autowired
	public SearchService(UtilService utilService, LoadService loadService) {
		this.utilService = utilService;
		this.loadService = loadService;
	}
	
	public SearchService() {}

	/**
	 * Simple method which just returns all Munro's unsorted
	 * @return a list of all the Munro objects
	 * @throws Exception
	 */
	public List<Munro> getAllMunros() throws Exception {

		setupMunroList();

		return loadService.getMunrosFromContext();

	}

	/**
	 * Main method for searching and sorting the Munro's
	 * @param cat the category which should be either TOP, MUN or EITHER 
	 * @param maHeight the maximum height of a Munro to return
	 * @param miHeight the minimum height of a Munro to return
	 * @param lim the limit of number of Munro's to return
	 * @param sBy sort by either NAME, HEIGHT, NONE, NAME_HEIGHT, HEIGHT_NAME
	 * @param sOrder sort order ASC, DESC, ASC_ASC, ASC_DESC, DESC_ASC, DESC_DESC
	 * @return
	 * @throws ApplicationException
	 */
	public List<Munro> searchMunros(String cat, String maHeight, String miHeight, String lim, String sBy,
			String sOrder) throws Exception {

		SearchParameters parameters = validateAndGenerateSearchParams(
				cat, maHeight, miHeight, lim, sBy, sOrder);

		setupMunroList();

		List<Munro> munros = loadService.getMunrosFromContext();

		logger.debug("Got the list of munro's from memory");

		List<Predicate<Munro>> searchPredicates = getSearchPredicates(parameters);

		Comparator<Munro> comparator = getSortComparator(parameters);

		int limit = munros.size();
		if (parameters.shouldLimitNumberResults()) {
			limit = parameters.getLimit();
		}

		logger.debug("Going to stream results now");

		return munros.stream()
				.filter(searchPredicates.stream().reduce(x->true, Predicate::and))
				.sorted(comparator)
				.limit(limit)
				.collect(Collectors.toList());
	}

	/**
	 * Utility method for getting the sort criteria based on sort criteria
	 * @param parameters
	 * @return
	 */
	private Comparator<Munro> getSortComparator(SearchParameters parameters) {
		Comparator<Munro> comparator = null;
		String sortBy = parameters.getSortBy();
		String sortOrder = parameters.getsortOrder();
		if (sortBy.equals(Constants.SORT_BY_NAME)) {
			comparator = getNameComparator();
			if (parameters.getsortOrder().equals(Constants.SORT_ORDER_DESC)) {
				comparator = comparator.reversed();
			}
		} else if (sortBy.equals(Constants.SORT_BY_HEIGHT)) {
			comparator = getHeightComparator();
			if (parameters.getsortOrder().equals(Constants.SORT_ORDER_DESC)) {
				comparator = comparator.reversed();
			}
		} else if (sortBy.equals(Constants.SORT_BY_HEIGHT_THEN_NAME)) {
			
			Comparator<Munro> sortByHeight = Comparator.comparing( Munro::getHeight );
			Comparator<Munro> sortByName = Comparator.comparing( Munro::getName, String.CASE_INSENSITIVE_ORDER );
			
			if (sortOrder.equals(Constants.SORT_ORDER_DESC_ASC) ||
					sortOrder.equals(Constants.SORT_ORDER_DESC_DESC)) {
				logger.debug("Need to reverse first comparator");
				sortByHeight = sortByHeight.reversed();
			}
			
			if (sortOrder.equals(Constants.SORT_ORDER_ASC_DESC) ||
					sortOrder.equals(Constants.SORT_ORDER_DESC_DESC)) {
				logger.debug("Need to reverse second comparator");
				sortByName = sortByName.reversed();
			}
			
			comparator = sortByHeight.thenComparing(sortByName);			
			
		} else if (sortBy.equals(Constants.SORT_BY_NAME_THEN_HEIGHT)) {
			Comparator<Munro> sortByName = Comparator.comparing( Munro::getName, String.CASE_INSENSITIVE_ORDER );
			Comparator<Munro> sortByHeight = Comparator.comparing( Munro::getHeight );

			if (sortOrder.equals(Constants.SORT_ORDER_DESC_ASC) ||
					sortOrder.equals(Constants.SORT_ORDER_DESC_DESC)) {
				logger.debug("Need to reverse first comparator");
				sortByName = sortByName.reversed();
			}
			
			if (sortOrder.equals(Constants.SORT_ORDER_ASC_DESC) ||
					sortOrder.equals(Constants.SORT_ORDER_DESC_DESC)) {
				logger.debug("Need to reverse second comparator");
				sortByHeight = sortByHeight.reversed();
			}
			
			comparator = sortByName.thenComparing(sortByHeight);	
		} else {
			logger.debug("Not going to sort");
			comparator = (Munro m1, Munro m2) -> 0 ;
		}

		return comparator;
	}
	
	private Comparator<Munro> getNameComparator() {
		return Comparator.comparing( Munro::getName, String.CASE_INSENSITIVE_ORDER );
	}
	
	private Comparator<Munro> getHeightComparator() {
		return Comparator.comparing( Munro::getHeight );
	}

	/**
	 * Utility method for getting the predicate to search the Munro's
	 * @param parameters
	 * @return a list of predicates to use within stream
	 */
	private List<Predicate<Munro>> getSearchPredicates(SearchParameters parameters) {
		List<Predicate<Munro>> allPredicates = new ArrayList<Predicate<Munro>>();

		if (parameters.shouldSearchByCategory()) {
			logger.debug("Searching for category {}", parameters.getCategory());
			allPredicates.add(mun -> mun.getCategoryCode().equals(parameters.getCategory()));
		}
		if (parameters.shouldSearchByMinHeight()) {
			logger.debug("Applying a min height of {}", parameters.getMinHeight());
			allPredicates.add(mun -> mun.getHeight() > parameters.getMinHeight());
		}
		if (parameters.shouldSearchByMaxHeight()) {
			logger.debug("Applying a max height of {}", parameters.getMaxHeight());
			allPredicates.add(mun -> mun.getHeight() < parameters.getMaxHeight());
		}

		return allPredicates;
	}

	/**
	 * Utility method which validates the input strings and then creates a SearchParameters
	 * object if they are valid
	 * @param category
	 * @param maxHeight
	 * @param minHeight
	 * @param limit
	 * @param sortBy
	 * @param sortOrder
	 * @return a SearchParameters object which contains the search and sort criteria
	 * @throws ApplicationException
	 */
	private SearchParameters validateAndGenerateSearchParams(String category, String maxHeight, String minHeight, String limit, String sortBy,
			String sortOrder) throws ApplicationException {

		if (!EnumUtils.isValidEnum(Category.class, category) ) {
			throw new ApplicationException("Error, invalid category supplied, should be TOP, MUNRO or EITHER");
		}

		if (StringUtils.hasText(maxHeight) && (!utilService.isValidDouble(maxHeight) 
				|| utilService.isNegative(utilService.getDoubleValue(maxHeight)))) {
			throw new ApplicationException("Error, max height supplied is not an integer");
		}

		if (StringUtils.hasText(minHeight) && (!utilService.isValidDouble(minHeight)
				|| utilService.isNegative(utilService.getDoubleValue(minHeight)))) {
			throw new ApplicationException("Error, min height supplied is not an integer");
		}

		if (StringUtils.hasText(limit) && (!utilService.isValidInteger(limit) 
				|| utilService.isNegative(utilService.getDoubleValue(limit)))) {
			throw new ApplicationException("Error, limit supplied is not an integer");
		}

		if (!EnumUtils.isValidEnum(SortBy.class, sortBy)) {
			throw new ApplicationException("Error, invalid sort by has been supplied, should be NAME, HEIGHT,"
					+ " NAME_HEIGHT, HEIGHT_NAME or NONE");
		}

		if (!sortBy.equals(Constants.SORT_BY_DEFAULT)) {
			// want to do a sort of some kind
			if (isComplexSearch(sortBy)) {
				logger.debug("Validating for a complex sort");
				if (!EnumUtils.isValidEnum(SortOrderComplex.class, sortOrder)) {
					throw new ApplicationException("Error, invalid sort order supplied for a complex sort");
				}
			} else {
				logger.debug("Validating for a simple sort");
				if (!EnumUtils.isValidEnum(SortOrder.class, sortOrder)) {
					throw new ApplicationException("Error, invalid sort order supplied");
				}
			}
		}

		SearchParameters paramters = new SearchParameters(category, utilService.getDoubleValue(maxHeight), 
				utilService.getDoubleValue(minHeight), utilService.getIntValue(limit), sortBy, sortOrder);

		logger.debug("Made SearchParameters object, now to do some extra validation");

		if (paramters.shouldSearchByMaxHeight() && paramters.shouldSearchByMinHeight()) {
			// want to search by both min and max, check the max is greater than the min
			if (paramters.getMaxHeight() < paramters.getMinHeight()) {
				throw new ApplicationException("Error, the max height cannot be less than the min height");
			}
		}

		return paramters;
	}

	/**
	 * Utility method which checks if Munro list exists in context, if not
	 * then it loads file
	 * @throws Exception
	 */
	private void setupMunroList() throws Exception {
		if (loadService.munrosStoredAlready()) {
			logger.debug("Munro's are already stored in context");
		} else {
			loadService.loadFromCsv();
		}
	}

	/**
	 * Utility method to check if doing complex search or not
	 * @param sortBy
	 * @return true if complex sort
	 */
	private boolean isComplexSearch(String sortBy) {
		if (sortBy.equals(Constants.SORT_BY_HEIGHT_THEN_NAME) ||
				sortBy.equals(Constants.SORT_BY_NAME_THEN_HEIGHT)) {
			return true;
		} else {
			return false;
		}

	}
}