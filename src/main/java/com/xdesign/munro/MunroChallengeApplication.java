package com.xdesign.munro;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.web.context.WebApplicationContext;

import com.xdesign.munro.model.ResponseObject;

import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Info;
import io.swagger.v3.oas.models.info.License;

@SpringBootApplication
public class MunroChallengeApplication {
	

	public static void main(String[] args) {
		SpringApplication.run(MunroChallengeApplication.class, args);
	}
	
	/**
	 * To avoid any thread issues, make the object of scope request
	 * @return
	 */
	@Bean
    @Scope(value = WebApplicationContext.SCOPE_REQUEST, proxyMode = ScopedProxyMode.TARGET_CLASS)
    public ResponseObject sjo() {
        return new ResponseObject();
    }
	
	@Bean
	public OpenAPI customOpenAPI() {
	    return new OpenAPI().info(new Info()
	      .title("Munro Challenge API")
	      .version("1.0.0")
	      .description("This API is used to solve XDesign's Munro Library Challenge")
	      .termsOfService("http://swagger.io/terms/")
	      .license(new License().name("Apache 2.0")
	      .url("http://springdoc.org")));
	}
	

}
