package com.xdesign.munro.controllers;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.xdesign.munro.exceptions.ApplicationException;
import com.xdesign.munro.model.ResponseObject;

public class ExceptionController {

	Logger logger = LoggerFactory.getLogger(MunroController.class);
	
	@ResponseStatus(value=HttpStatus.INTERNAL_SERVER_ERROR)
	@ExceptionHandler(IOException.class)
    public @ResponseBody ResponseObject handleIOException(HttpServletRequest request, Exception ex) {
        logger.error("IOException Occured:: URL={}", request.getRequestURL());
        return generateResponse("IOException has occurred", ex);
    }
    
    @ResponseStatus(value=HttpStatus.INTERNAL_SERVER_ERROR)
    @ExceptionHandler(Exception.class)
    public @ResponseBody ResponseObject handleGeneralException(HttpServletRequest request, Exception ex) {
        logger.error("Exception Occured:: URL={}", request.getRequestURL());
        // this should catch any other exceptions
        
        return generateResponse( "An exception has occurred", ex);
    }
    
    @ResponseStatus(value=HttpStatus.BAD_REQUEST)
    @ExceptionHandler(ApplicationException.class)
    public @ResponseBody ResponseObject handleApplicationException(HttpServletRequest request, Exception ex) {
        logger.error("Application Exception Occured:: URL={}", request.getRequestURL());
        return generateResponse(ex);
    }
   
    private ResponseObject generateResponse(String type, Exception ex) {
    	ResponseObject ro = new ResponseObject();
        logger.error(type, ex);
        ro.setData(type);
        return ro;
    }
    
    private ResponseObject generateResponse(Exception ex) {
    	ResponseObject ro = new ResponseObject();
    	ro.setData(ex.getMessage());
        return ro;
    }
    
}
