package com.xdesign.munro.controllers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.xdesign.munro.model.Constants;
import com.xdesign.munro.model.Munro;
import com.xdesign.munro.model.ResponseObject;
import com.xdesign.munro.services.SearchService;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;

@RestController
@RequestMapping("/")
@Tag(name = "Munro Finder", description = "The API's available for discovering Munro's")
public class MunroController extends ExceptionController {

	Logger logger = LoggerFactory.getLogger(MunroController.class);

	private ResponseObject ro;
	private SearchService searchService;

	@Autowired
	public MunroController(ResponseObject ro, SearchService searchService) {
		this.ro = ro;
		this.searchService = searchService;
	}

	@Operation(summary = "Get all Munro's as they are entered in CSV file")
	@ApiResponses(value = {
			@ApiResponse(responseCode = "200", description = "Gives an unsorted list of all Munro's", content = { 
					@Content(mediaType = "application/json", schema = @Schema(implementation = Munro.class))}) })
	@GetMapping("munros")
	public ResponseObject getAllMunros() throws Exception {
		logger.debug("Going to get all munros");
		ro.setData(searchService.getAllMunros());
		return ro;
	}

	@Operation(summary = "Get all Munro's based on search criteria")
	@ApiResponses(value = {
			@ApiResponse(responseCode = "200", description = "Gives a sorted/unsorted filtered list of all Munro's", content = { 
					@Content(mediaType = "application/json", schema = @Schema(implementation = Munro.class))}) })
	@GetMapping("/searchmunros")
	public ResponseObject searchMunros (
			@Parameter(description = "Category as of 1997, TOP, MUN, EITHER are possible values") 
			@RequestParam(name = "category", defaultValue = Constants.CAT_ANY) String category,
			@Parameter(description = "Max height of munro in metres")
			@RequestParam(name = "maxheight", defaultValue = "0") String maxHeight,
			@Parameter(description = "Min height of munro in metres")
			@RequestParam(name = "minheight", defaultValue = "0") String minHeight,
			@Parameter(description = "Maximum number of munro's to return")
			@RequestParam(name = "limit", defaultValue = "0") String limit,
			@Parameter(description = "Sort results by NAME or HEIGHT. No sorting if not supplied. To sort by both"
					+ " enter with an underline between like 'NAME_HEIGHT' to sort by name then height")
			@RequestParam(name = "sortby", defaultValue = Constants.SORT_BY_DEFAULT) String sortBy,
			@Parameter(description = "Sort results ASC or DESC. If sorting by both, you can specify sorting "
					+ "name ASC and height DESC by 'ASC_DESC'")
			@RequestParam(name = "sortorder", defaultValue = Constants.SORT_ORDER_ASC) String sortOrder
			) throws Exception {
		
		logger.debug("Search params are, category:{}, maxheight:{}, minheight:{}, "
				+ "limit:{}, sortBy:{}, sortOrder:{}", category, maxHeight, minHeight, limit, sortBy, sortOrder);

		ro.setData(searchService.searchMunros(category, maxHeight, minHeight, limit, sortBy, sortOrder));
		return ro;
	}



}
