package com.xdesign.munro.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties({"targetClass", "targetSource", "targetObject", "advisors","advisorCount", "frozen", "exposeProxy", "preFiltered", "proxiedInterfaces", "proxyTargetClass"}) 
public class ResponseObject {

	Object data = "";

	public ResponseObject() {}

	public ResponseObject(Object data) {
		this.data = data;
	}

	public Object getData() {
		return data;
	}

	public void setData(Object data) {
		this.data = data;
	}
	
	

}
