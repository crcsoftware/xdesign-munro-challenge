package com.xdesign.munro.model;

public enum SortOrder {

	ASC, DESC
}
