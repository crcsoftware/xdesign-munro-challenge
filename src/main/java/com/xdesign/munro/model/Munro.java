package com.xdesign.munro.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class Munro {

	private String name = "";
	private String gridReference = "";
	private String category = "";
	private double height = 0;

	public Munro() {}

	public Munro(String name, String gridReference, String category, double height) {
		this.name = name;
		this.gridReference = gridReference;
		this.category = category;
		this.height = height;
	}

	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getGridReference() {
		return gridReference;
	}
	public void setGridReference(String gridReference) {
		this.gridReference = gridReference;
	}
	public String getCategory() {
		if (category.equals(Constants.CAT_MUNRO)) {
			return "Munro";
		} else {
			return "Munro Top";
		}
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public double getHeight() {
		return height;
	}
	public void setHeight(double height) {
		this.height = height;
	}
	@JsonIgnore
	public String getCategoryCode() {
		return this.category;
	}



}
