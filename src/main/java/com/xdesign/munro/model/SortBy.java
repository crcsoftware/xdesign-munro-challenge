package com.xdesign.munro.model;

public enum SortBy {

	NAME, HEIGHT, NONE, NAME_HEIGHT, HEIGHT_NAME
}
