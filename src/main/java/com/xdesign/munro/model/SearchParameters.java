package com.xdesign.munro.model;

public class SearchParameters {

	private String category = Constants.CAT_ANY; // TOP, MUN, EITHER
	private double maxHeight = 0;
	private double minHeight = 0;
	private int limit = 0;
	private String sortBy = ""; // name or height
	private String sortOrder = ""; // asc or desc

	public SearchParameters() {}
	
	public SearchParameters(String category, double maxHeight, double minHeight, int limit, String sortBy, String sortOrder) {
		this.category = category;
		this.maxHeight = maxHeight;
		this.minHeight = minHeight;
		this.limit = limit;
		this.sortBy = sortBy;
		this.sortOrder = sortOrder;
	}
	
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public double getMaxHeight() {
		return maxHeight;
	}
	public void setMaxHeight(double maxHeight) {
		this.maxHeight = maxHeight;
	}
	public double getMinHeight() {
		return minHeight;
	}
	public void setMinHeight(double minHeight) {
		this.minHeight = minHeight;
	}
	public int getLimit() {
		return limit;
	}
	public void setLimit(int limit) {
		this.limit = limit;
	}
	public String getSortBy() {
		return sortBy;
	}
	public void setSortBy(String sortBy) {
		this.sortBy = sortBy;
	}
	public String getsortOrder() {
		return sortOrder;
	}
	public void setsortOrder(String sortOrder) {
		this.sortOrder = sortOrder;
	}

	public boolean shouldSearchByMinHeight() {
		if (this.minHeight != 0) {
			return true;
		} else {
			return false;
		}
	}

	public boolean shouldSearchByMaxHeight() {
		if (this.maxHeight != 0) {
			return true;
		} else {
			return false;
		}
	}

	public boolean shouldLimitNumberResults() {
		if (this.limit != 0) {
			return true;
		} else {
			return false;
		}
	}
	
	public boolean shouldSearchByCategory() {
		if (this.category.equals(Constants.CAT_ANY)) {
			return false;
		} else {
			return true;
		}
	}
	
	public boolean needToSearch() {
		if (shouldSearchByCategory() || shouldSearchByMaxHeight() || shouldSearchByMinHeight()) {
			return true;
		} else {
			return false;
		}
	}


}
