package com.xdesign.munro.model;

public class Constants {

	// Category
	public final static String CAT_MUNRO = "MUN";
	public final static String CAT_MUNRO_TOP = "TOP";
	public final static String CAT_ANY = "EITHER";
	
	// SortBy
	public final static String SORT_BY_NAME = "NAME";
	public final static String SORT_BY_HEIGHT = "HEIGHT";
	public final static String SORT_BY_DEFAULT = "NONE";
	public final static String SORT_BY_NAME_THEN_HEIGHT = "NAME_HEIGHT";
	public final static String SORT_BY_HEIGHT_THEN_NAME = "HEIGHT_NAME";
		
	// SortType
	public final static String SORT_ORDER_ASC = "ASC";
	public final static String SORT_ORDER_DESC = "DESC";
	public final static String SORT_ORDER_ASC_ASC = "ASC_ASC";
	public final static String SORT_ORDER_ASC_DESC = "ASC_DESC";
	public final static String SORT_ORDER_DESC_ASC = "DESC_ASC";
	public final static String SORT_ORDER_DESC_DESC = "DESC_DESC";
				
	public final static String CSV_HEADING_NAME = "Name";
	public final static String CSV_HEADING_CAT = "Post 1997";
	public final static String CSV_HEADING_GRID = "Grid Ref";
	public final static String CSV_HEADING_HEIGHT = "Height (m)";
			
	// stored Munro's
	public final static String STORED_MUNROS = "com.xdesign.munro.storedmunros";
}
