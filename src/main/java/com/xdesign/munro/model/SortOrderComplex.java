package com.xdesign.munro.model;

public enum SortOrderComplex {

	ASC_ASC, ASC_DESC, DESC_ASC, DESC_DESC
}
